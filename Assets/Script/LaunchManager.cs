﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;

    private void Awake()
    {
        // this'll make all clients load the scene that the master client has loaded
        PhotonNetwork.AutomaticallySyncScene = true; 
    }

    // Start is called before the first frame update
    void Start()
    {
        EnterGamePanel.SetActive(true); // set this as default screen when starting up
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // called whenever game is connected to the server
    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + " Connected to Server");

        // upon connecting, disable connecting panel then enable the lobby panel
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
    }

    // called when Photon detects we are connected to the internet
    public override void OnConnected()
    {
        Debug.Log("Connected to Internet");
    }

    // this fx will be called when no match was found
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);
        CreateAndJoinRoom();
    }

    public void ConnectToPhotonServer()
    { 
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings(); // use to connect to Photon server when starting game
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom(); // join a random room
    }

    // this fx will be called when OnJoinRandomFailed is called
    private void CreateAndJoinRoom()
    {
        string randomRoomName = "Room " + Random.Range(0, 10000); // set random name

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
    }

    
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + " has entered room " +
                    PhotonNetwork.CurrentRoom.Name + ". Room has now " + 
                    PhotonNetwork.CurrentRoom.PlayerCount + " players");
    }

}
